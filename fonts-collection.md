# Quelques fonderies et collections 



## Fonderies libres

[Cat fonts](http://www.peter-wiegel.de/)  
[Collletttivo](http://collletttivo.it/)  
[Fonderie downlaod](https://www.fonderie.download/)  
[Fonderiz](https://fonderiz.fr/)  
[Hanken](https://hanken.co)  
[Impallari Type](https://github.com/impallari?tab=repositories)  
[Love letters](http://www.love-letters.be/)  
[Not your type](https://sdfggvfvj.github.io/notyourtype/)  
[OSP foundry](http://osp.kitchen/foundry/)  
[Phantom foundry](http://phantom-foundry.com/)  
[Tunera](http://www.tunera.xyz/)  
[Warsaw type](https://kroje.org/en/)  
[Un japonais, des fontes simples, complètes](http://dotcolon.net)
[VTF](https://velvetyne.fr/) Velvetyne Type Foundry  
[Workshop typothèque](https://workshops.luuse.fun/_workshop_typotheque/) Luuse


## Collections libres

[Badass Libre Fonts by Womxn](https://www.design-research.be/by-womxn)  
Cette collection vise à donner de la visibilité aux fontes libres dessinées par des designers féminins, qui sont souvent sous-représentées dans le domaine traditionnellement conservateur de la typographie. 

https://annuel.framapad.org/p/fonteslibres
Sélection restreinte de fontes libres de la HEAR + documentation

[Uncut](https://uncut.wtf/)  
[la typothèque de Luuse](http://typotheque.luuse.io/)  
[Velvetyne](https://www.velvetyne.fr)   

[Use and modify de Raphaël Bastide](https://usemodify.com) 
"Ceci est une sélection personnelle de caractères beaux, classes, punks, professionnels, incomplets, bizarres. Les licences open source les rendent libres d'utilisation et de modification. Cette sélection est le résultat de recherches profondes et de coups de cœur. Cette sélection est la vôtre."  

[Fontain par Lakkon](https://fontain.org/)  
[Font Squirrel](https://www.fontsquirrel.com)  
[Open Foundry](https://open-foundry.com)  
[The league of moveable type](https://www.theleagueofmoveabletype.com/)  
[Font Library](https://fontlibrary.org)  
[Goofle fonts](https://fonts.google.com)  
[La typothèque de TeX](https://www.tug.org/FontCatalogue/allfonts.html)  
[Tunera](http://www.tunera.xyz/)  


*Avec un focus monospaces*

[Codeface](https://github.com/chrissimpkins/codeface)

[The anatomy of a Thousand Typefaces](https://useratio.com/the-anatomy-of-a-thousand-typefaces/app/)


[Victor mono](https://rubjo.github.io/victor-mono/)



## Schools!

[U+270D](https://u270d.eesab.fr) à l'EESAB - Rennes

[SUVA](https://www.suvatypefoundry.ee/) à Talinn, Estonie 

[Chasse ouverte](http://chasse-ouverte.erg.be), la fonderie de l'ERG 

[ANRT](https://anrt-nancy.fr/en/fonts-en/) 

[La typothèque du 75](http://typotheque.le75.be/) 

[ECAL](https://ecal-typefaces.ch/) 

[ESAD Amiens](http://postdiplome.esad-amiens.fr/) 

[Reading university](http://typefacedesign.net/typefaces/) 

[No foundry](http://nofoundry.xyz)   
Visual Communication department at HfG Karlsruhe “NoFoundry is a website for student type designers, enabling them to exchange ideas and sell fonts on their own terms. ” 

[La Cambre](http://lacambretypo.be/fr) 

[KABK Master type and media](https://www.kabk.nl/en/programmes/master/type-and-media)  https://www.typemedia2018.com/

[E162](http://e162.eu/fonderie), fonderie d'Estienne 

[2000 upm](https://2000upm.uk/), une initiative d'étudian* pour financer leur jury de diplôme (Kingston School of Art, Kingston Upon Thames, UK)


[Single line](https://www.singlelinefonts.com/collections/svg-fonts) isdat Toulouse 


## Entre deux

[Future Fonts](https://www.futurefonts.xyz/) 

[Noir Blanc Rouge](https://noirblancrouge.com) 

[Les fontes libres](http://typefaces.temporarystate.net/preview/) de Roman Gornitsky de The Temporary State , juste à côté des privatives et aussi [les textes](http://letters.temporarystate.net/) 



## Independent type foundries

[Grillitype](https://www.grillitype.com/) par exemple https://www.gt-maru.com/

[BB-Bureau](https://www.bb-bureau.fr/) 

OurType Belgium-based type foundry established in 2002 by Fred Smeijers.

[Bold Monday](https://boldmonday.com/) 

[The designers foundry](https://thedesignersfoundry.com/fragen)  

[Lineto](https://lineto.com/) 

[Colophon Foundry](https://www.colophon-foundry.org/) (us) 

[P22](https://p22.com/) 

Hoefler & Co. https://www.typography.com/

Klim Type Foundry (New Zealand) https://klim.co.nz/

[Or type](https://ortype.is/) (Bruxelles from danemark/islande) 

[A2-Type](www.a2-type.co.uk) (London) 

[Swiss Typefaces](https://www.swisstypefaces.com) 

[Black foundry](https://black-foundry.com/fonts/) 

L'imprimerie nationale de France https://twitter.com/ANRT_type/status/1186253028434874368)

[Plain form](https://plain-form.com/)
    
[Paratype](https://www.paratype.com/)

    
[Out of the dark](https://www.outofthedark.xyz/)




## Independants

[A is for fonts](https://aisforfonts.com/fonts) Émile Rigaud 

[Roxane Gataud](https://www.roxanegataud.com/) 

[Studio Cryo](https://berzulis.com/), Lithuanian mythology and alphabet

[Lucas Descroix](https://www.lucasdescroix.fr/) 

[Radim Peško](https://radimpesko.com/) aka RP Digital Type Foundry  

Justin Bihan, [Lettres simples](http://www.lettres-simples.com/) 

[Yann Linguinou](https://yannlinguinou.com/) 

[Lucas le Bihan](https://bretagnebretagne.fr/) 

[Eliott Grunewald](https://eliottgrunewald.xyz/) 

Erik Marinovich, [Nuform](https://nuformtype.com/) 

[Glyphworld](http://www.glyphworld.online/) Leah Maldonado 






## Meta meta : outils

    "Ofont is a free and open source tool for font collections. It runs in the browser and is easy to install on your server.

    Possible usages : foundry, microfoundry, font portfolio, to share your favorites fonts!"

    https://github.com/raphaelbastide/ofont
